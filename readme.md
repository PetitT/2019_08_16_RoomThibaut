# How to use: 2019_08_16_RoomThibaut   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.technifutur.roomthibaut":"https://gitlab.com/PetitT/2019_08_16_RoomThibaut.git",    
```    
--------------------------------------    
   
Feel free to support my work:    
Contact me if you need assistance:    
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.technifutur.roomthibaut",                              
  "displayName": "RoomThibaut",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Une jolie pièce :)",                         
  "keywords": ["Tentacule"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    