﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ColorChange();
    }

    private void ColorChange()
    {
        gameObject.GetComponentInChildren<Renderer>().material.color = new Color(Random.value, Random.value, Random.value);
    }
}
